#include "../src/Solve.h"
#include "gtest/gtest.h"

TEST(YandexTest, FirstTest) {
  std::string e = "00?";
  std::string p = "1?";
  Solve solve;
  ASSERT_EQ(solve.parseStrings(e, p), 2);
  ASSERT_EQ(solve.checkStrings(e, p), 2);
}

TEST(MyTest, FirstTest) {
  std::string e = "?1?1?1?1?";
  std::string p = "?1?1?";
  Solve solve;
  ASSERT_EQ(solve.parseStrings(e, p), 0);
  ASSERT_EQ(solve.checkStrings(e, p), 0);
}

TEST(MyTest, SecondTest) {
  std::string e = "1010101";
  std::string p = "1?1?1";
  Solve solve;
  ASSERT_EQ(solve.parseStrings(e, p), 5);
  ASSERT_EQ(solve.checkStrings(e, p), 5);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}