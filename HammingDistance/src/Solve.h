#ifndef HAMMINGDISTANCE_SOLVE_H
#define HAMMINGDISTANCE_SOLVE_H

#include "OneZeroCapGraph.h"

class Solve {
 public:
  static int checkStrings(const std::string& e, const std::string& p);
  int parseStrings(std::string& expression, std::string& pattern);
 private:
  std::string expression_;
  std::string pattern_;
  OneZeroCapGraph graph_ = OneZeroCapGraph(0);

  void replaceStrings();
};

#endif //HAMMINGDISTANCE_SOLVE_H
