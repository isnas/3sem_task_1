#include "Solve.h"

int Solve::checkStrings(const std::string &e, const std::string &p) {
  int counter = 0;
  for (size_t pPointer = 0; pPointer < p.size(); ++pPointer) {
    for (size_t ePointer = 0; ePointer < e.size() - p.size() + 1; ++ePointer) {
      char first = e[pPointer + ePointer];
      char second = p[pPointer];

      switch (first) {
        case '1':
          switch (second) {
            case '0':
              ++counter;
            case '1':
              continue;
            default:
              return -1;
          }
        case '0':
          switch (second) {
            case '1':
              ++counter;
            case '0':
              continue;
            default:
              return -1;
          }
        default:
          return -1;
      }
    }
  }
  return counter;
}

int Solve::parseStrings(std::string &expression, std::string &pattern) {
  assert(expression.size() >= pattern.size());
  expression_ = expression;
  pattern_ = pattern;

  size_t zeroVertex = 0;
  size_t unitVertex = expression.size() + pattern.size() + 1;
  graph_ = OneZeroCapGraph(unitVertex + 1);

  for (size_t patternPointer = 0; patternPointer < pattern.size(); ++patternPointer) {
    for (size_t expressionPointer = 0; expressionPointer < expression.size() - pattern.size() + 1; ++expressionPointer) {
      char first = expression[patternPointer + expressionPointer];
      char second = pattern[patternPointer];

      size_t firstPosition = patternPointer + expressionPointer + 1;
      size_t secondPosition = expression.size() + patternPointer + 1;

      switch (first) {
        case '?':
          switch (second) {
            case '?':
              graph_.addEdge(firstPosition, secondPosition);
              graph_.addEdge(secondPosition, firstPosition);
              continue;
            case '1':
              graph_.addEdge(firstPosition, unitVertex);
              continue;
            case '0':
              graph_.addEdge(zeroVertex, firstPosition);
              continue;
            default:
              assert(false);
          }
        case '1':
          switch (second) {
            case '?':
              graph_.addEdge(secondPosition, unitVertex);
              continue;
            case '1':
              continue;
            case '0':
              graph_.addEdge(zeroVertex, unitVertex);
              continue;
            default:
              assert(false);
          }
        case '0':
          switch (second) {
            case '?':
              graph_.addEdge(zeroVertex, secondPosition);
              continue;
            case '1':
              graph_.addEdge(zeroVertex, unitVertex);
              continue;
            case '0':
              continue;
            default:
              assert(false);
          }
        default:
          assert(false);
      }
    }
  }
  int size = graph_.dinic(zeroVertex, unitVertex);
  replaceStrings();

  expression = expression_;
  pattern = pattern_;
  return size;
}

void Solve::replaceStrings() {
  std::queue<size_t> vertexes;
  vertexes.push(graph_.startVertex_);
  graph_.distances_ = std::vector<int>(graph_.adjacencyList_.size(), -1);
  graph_.distances_[graph_.startVertex_] = 0;

  while (!vertexes.empty()) {
    size_t parent = vertexes.front();
    vertexes.pop();

    for (int i = 0; i < graph_.adjacencyList_[parent].size(); ++i) {
      size_t child = graph_.adjacencyList_[parent][i].next_;
      if (graph_.distances_[child] == -1 &&
          graph_.adjacencyList_[parent][i].flow_ < graph_.adjacencyList_[parent][i].capacity_) {
        graph_.distances_[child] = graph_.distances_[parent] + 1;
        vertexes.push(child);

        if (child - 1 < expression_.size())
          expression_[child - 1] = '0';
        else
          pattern_[child - 1 - expression_.size()] = '0';
      }
    }
  }

  for (size_t expressionPointer = 0; expressionPointer < expression_.size(); ++expressionPointer) {
    if (expression_[expressionPointer] == '?')
      expression_[expressionPointer] = '1';
  }
  for (size_t patternPointer = 0; patternPointer < pattern_.size(); ++patternPointer) {
    if (pattern_[patternPointer] == '?')
      pattern_[patternPointer] = '1';
  }
}