#include "OneZeroCapGraph.h"

void OneZeroCapGraph::addEdge(size_t start, size_t end) {
  assert(start >= 0 && start < adjacencyList_.size());
  assert(end >= 0 && end < adjacencyList_.size());

  adjacencyList_[start].push_back(OneZeroEdge(end, 1));
  adjacencyList_[end].push_back(OneZeroEdge(start, 0));
  adjacencyList_[start].back().numberOfReverseEdge_ = adjacencyList_[end].size() - 1;
  adjacencyList_[end].back().numberOfReverseEdge_ = adjacencyList_[start].size() - 1;
}

int OneZeroCapGraph::dinic(size_t start, size_t end) {
  assert(start >= 0 && start < adjacencyList_.size());
  assert(end >= 0 && end < adjacencyList_.size());
  startVertex_ = start;
  endVertex_ = end;

  int flowSize = 0;
  int blockFlow = 0;
  while (true) {
    if (!bfs())
      break;
    std::vector<size_t> passableVertexPointer(adjacencyList_.size(), 0);
    do {
      blockFlow = dfs(startVertex_, INT_MAX, passableVertexPointer);
      flowSize += blockFlow;
    } while (blockFlow != 0);
  }
  return flowSize;
}

int OneZeroCapGraph::dfs(size_t vertex, int flow, std::vector <size_t> &passableVertexPointer) {
  if (flow == 0 || vertex == endVertex_)
    return flow;

  for (size_t& i = passableVertexPointer[vertex]; i < adjacencyList_[vertex].size(); ++i) {
    size_t next = adjacencyList_[vertex][i].next_;

    if (distances_[next] != distances_[vertex] + 1)
      continue;
    int nextFlow = flow;
    if (nextFlow > adjacencyList_[vertex][i].capacity_ - adjacencyList_[vertex][i].flow_)
      nextFlow = adjacencyList_[vertex][i].capacity_ - adjacencyList_[vertex][i].flow_;

    int flowUnit = dfs(next, nextFlow, passableVertexPointer);
    if (flowUnit == 1) {
      size_t reverseEdge = adjacencyList_[vertex][i].numberOfReverseEdge_;
      adjacencyList_[vertex][i].flow_ += flowUnit;
      adjacencyList_[next][reverseEdge].flow_ -= flowUnit;
      return flowUnit;
    }
  }
  return 0;
}

bool OneZeroCapGraph::bfs() {
  std::queue<size_t> vertexes;
  vertexes.push(startVertex_);
  distances_ = std::vector<int>(adjacencyList_.size(), -1);
  distances_[startVertex_] = 0;

  while (!vertexes.empty()) {
    size_t parent = vertexes.front();
    vertexes.pop();

    for (int i = 0; i < adjacencyList_[parent].size(); ++i) {
      size_t child = adjacencyList_[parent][i].next_;
      if (distances_[child] == -1 && adjacencyList_[parent][i].flow_ < adjacencyList_[parent][i].capacity_) {
        distances_[child] = distances_[parent] + 1;
        vertexes.push(child);
      }
    }
  }
  return distances_[endVertex_] != -1;
}