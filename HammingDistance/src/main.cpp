#include "Solve.h"


int main() {
  std::string s;
  std::string p;
  Solve solve;
  std::cin >> s >> p;
  std::cout << solve.parseStrings(s, p) << std::endl;
  std::cout << s << std::endl;
  std::cout << p << std::endl;
  return 0;
}