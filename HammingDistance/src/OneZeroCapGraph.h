#ifndef HAMMINGDISTANCE_ONEZEROCAPGRAPH_H
#define HAMMINGDISTANCE_ONEZEROCAPGRAPH_H

#include <iostream>
#include <assert.h>
#include <queue>
#include <limits.h>

class OneZeroCapGraph {
 public:
  OneZeroCapGraph(size_t size) {
    adjacencyList_ = std::vector<std::vector<OneZeroEdge>>(size);
  }
  void addEdge(size_t start, size_t end);
  int dinic(size_t start, size_t end);

 private:
  struct OneZeroEdge {
    OneZeroEdge(size_t next, int capacity) : next_(next), capacity_(capacity) {
      assert(capacity_ == 0 || capacity_ == 1);
      flow_ = 0;
      numberOfReverseEdge_ = SIZE_MAX;
    }

    size_t next_;
    int flow_;
    int capacity_;
    size_t numberOfReverseEdge_;
  };

  std::vector<std::vector<OneZeroEdge>> adjacencyList_;
  std::vector<int> distances_;
  size_t startVertex_, endVertex_;

  int dfs(size_t vertex, int flow, std::vector<size_t>& passableVertexPointer);

  bool bfs();

  friend class Solve;
};

#endif //HAMMINGDISTANCE_ONEZEROCAPGRAPH_H
