#ifndef TRANSFORMATION_TRANSFORM_H
#define TRANSFORMATION_TRANSFORM_H

#include <vector>
#include <string>

static const std::string ALPHABET = "abcdefghijklmnopqrstuvwxyz";

class Transform {
 public:
  static std::vector<int> strToZFunc       (const std::string& str);
  static std::vector<int> strToPrefixFunc  (const std::string& str);
  static std::vector<int> ZFuncToPrefixFunc(const std::vector<int>& ZFunc);
  static std::vector<int> prefixFuncToZFunc(const std::vector<int>& prefixFunc);
  static std::string      prefixFuncToStr  (const std::vector<int>& prefixFunc);
  static std::string      ZFuncToStr       (const std::vector<int>& ZFunc);

  std::vector<int> entriesOfSubstring(const std::string& pattern, const std::string& substring);
 private:
  static int getIndex(char letter);
};

#endif //TRANSFORMATION_TRANSFORM_H
