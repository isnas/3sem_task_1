#include <iostream>
#include <vector>
#include "Transform.h"

int main() {
  std::string pattern;
  std::string string;
  std::cin >> pattern >> string;
  std::vector<int> output = Transform::strToZFunc(pattern + "#" + string);

  for (int i = 0; i < output.size(); ++i) {
    if (output[i] == pattern.size())
      std::cout << i - pattern.size() - 1 << ' ';
  }
  std::cout << std::endl;
  return 0;
}