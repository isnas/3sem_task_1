#include "Transform.h"

std::vector<int> Transform::strToZFunc(const std::string &str) {
  std::vector<int> ZFunc(str.size(), 0);
  std::pair<int, int> coincidence(0, 0);

  for (int current = 1; current < str.size(); ++current) {
    int leftConformChar = current - coincidence.first;
    int maxCorrectCharsInCoincidence = ZFunc[leftConformChar];
    int correctCharsLimiterInCoincidence = coincidence.second - current + 1;
    ZFunc[current] = std::min(correctCharsLimiterInCoincidence, maxCorrectCharsInCoincidence);

    if (ZFunc[current] < 0)
      ZFunc[current] = 0;

    int offset = ZFunc[current];
    while (current + offset < str.size() && str[current + offset] == str [offset])
      ++offset;

    ZFunc[current] = offset;
    if (offset + current > coincidence.second)
      coincidence = std::make_pair(current, current + offset - 1);
  }
  return ZFunc;
}

std::vector<int> Transform::strToPrefixFunc(const std::string &str) {
  std::vector<int> prefixFunc(str.size(), 0);
  for (int right = 1; right < prefixFunc.size(); ++right) {
    int oppositeCharIndex = prefixFunc[right - 1];
    while (oppositeCharIndex > 0 && str[right] != str[oppositeCharIndex])
      oppositeCharIndex = prefixFunc[oppositeCharIndex - 1];

    if (str[right] == str[oppositeCharIndex])
      ++oppositeCharIndex;
    prefixFunc[right] = oppositeCharIndex;
  }
  return prefixFunc;
}

std::vector<int> Transform::ZFuncToPrefixFunc(const std::vector<int> &ZFunc) {
  std::vector<int> prefixFunc(ZFunc.size(), 0);
  for (int left = 1; left < prefixFunc.size(); ++left) {
    for (int offset = ZFunc[left] - 1; offset >= 0; --offset) {
      if (prefixFunc[left + offset] > 0)
        break;

      int subStrLen = offset + 1;
      prefixFunc[left + offset] = subStrLen;
    }
  }
  return prefixFunc;
}

std::string Transform::prefixFuncToStr(const std::vector<int> &prefixFunc) {
  std::string str(prefixFunc.size(), '?');
  std::vector<bool> used(prefixFunc.size());
  str[0] = 'a';
  used[0] = true;
  int charUsed = 1;
  for (int index = 1; index < prefixFunc.size(); ++index) {
    if (prefixFunc[index] == 0) {
      std::fill(used.begin() + 1, used.begin() + charUsed, false);
      int oppositeCharIndex = prefixFunc[index - 1];
      while (oppositeCharIndex > 0) {
        char oppositeChar = str[oppositeCharIndex];
        used[getIndex(oppositeChar)] = true;
        oppositeCharIndex = prefixFunc[oppositeCharIndex - 1];
      }
      for (int alphabetIndex = 1; alphabetIndex < charUsed + 1; ++alphabetIndex) {
        if (!used[alphabetIndex]) {
          str[index] = ALPHABET[alphabetIndex];
          ++charUsed;
          break;
        }
      }
      if (str[index] == '?')
        str[index] = ALPHABET[charUsed++];
    } else {
      int oppositeCharIndex = prefixFunc[index] - 1;
      str[index] = str[oppositeCharIndex];
    }
  }
  return str;
}

std::vector<int> Transform::prefixFuncToZFunc(const std::vector<int> &prefixFunc) {
  return strToZFunc(prefixFuncToStr(prefixFunc));
}

std::string Transform::ZFuncToStr(const std::vector<int> &ZFunc) {
  return prefixFuncToStr(ZFuncToPrefixFunc(ZFunc));
}

int Transform::getIndex(char letter) {
  return letter - 'a';
}

std::vector<int> Transform::entriesOfSubstring(const std::string &pattern, const std::string &substring) {
  std::vector<int> counter = Transform::strToZFunc(pattern + "#" + substring);
  std::vector<int> output;

  for (int i = 0; i < output.size(); ++i) {
    if (counter[i] == pattern.size())
      output.push_back(i - pattern.size() - 1);
  }
  return output;
}