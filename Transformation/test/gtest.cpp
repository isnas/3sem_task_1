#include "../src/Transform.h"
#include "gtest/gtest.h"


TEST(MyTest, FirstTest) {
  std::string str = "aabcacbaababcaaacbabababaaaabbcbccbabccaaabca";
  ASSERT_EQ(Transform::strToPrefixFunc(str), Transform::ZFuncToPrefixFunc(Transform::strToZFunc(str)));
}

TEST(MyTest, SecondTest) {
  std::string str = "qwertqwertqwerqtwreqqrwetqrwetqqrweqtwerqtwer";
  ASSERT_EQ(Transform::strToPrefixFunc(str), Transform::ZFuncToPrefixFunc(Transform::strToZFunc(str)));
}

TEST(MyTest, ThirdTest) {
  std::string str = "abacabadabacabae";
  ASSERT_EQ(str, Transform::prefixFuncToStr(Transform::ZFuncToPrefixFunc(Transform::strToZFunc(str))));
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}