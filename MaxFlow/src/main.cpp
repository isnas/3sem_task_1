#include <iostream>
#include "Graph.h"

int main() {
  int size = 0;
  std::cin >> size;

  while (size != 0) {
    Graph g(size);

    int start, end;
    std::cin >> start >> end;
    start -= 1;
    end -= 1;
    int edgesCount = 0;
    std::cin >> edgesCount;

    int first, second, c;
    for (int i = 0; i < edgesCount; i++) {
      std::cin >> first >> second >> c;
      g.addEdge(first - 1, second - 1, c);
    }

    std::cout << g.dinic(start, end) << std::endl;
    std::cin >> size;
  }

  return 0;
}