#include "Graph.h"

int Graph::dinic(int start, int end) {
  startVertex_ = start;
  endVertex_ = end;

  int flowSize = 0;
  int blockFlow = 0;
  while (true) {
    if (!bfs())
      break;
    std::vector<int> passableVertexPointer(adjacencyMatrix_.size(), 0);
    do {
      blockFlow = dfs(startVertex_, INT_MAX, passableVertexPointer);
      flowSize += blockFlow;
    } while (blockFlow != 0);
  }
  return flowSize;
}

void Graph::addEdge(int start, int end, int capacity, bool isDirected) {
  assert(start >= 0 && start < adjacencyMatrix_.size());
  assert(end >= 0 && end < adjacencyMatrix_.size());

  adjacencyMatrix_[start][end].capacity_ += capacity;
  if (!isDirected)
    adjacencyMatrix_[end][start].capacity_ += capacity;
}

int Graph::dfs(int vertex, int flow, std::vector<int>& passableVertexPointer) {
  if (flow == 0 || vertex == endVertex_)
    return flow;

  for (int& next = passableVertexPointer[vertex]; next < adjacencyMatrix_[vertex].size(); ++next) {

    if (distances_[next] != distances_[vertex] + 1)
      continue;
    int nextFlow = flow;
    if (nextFlow > adjacencyMatrix_[vertex][next].capacity_ - adjacencyMatrix_[vertex][next].flow_)
      nextFlow = adjacencyMatrix_[vertex][next].capacity_ - adjacencyMatrix_[vertex][next].flow_;

    int flowUnit = dfs(next, nextFlow, passableVertexPointer);
    if (flowUnit > 0) {
      adjacencyMatrix_[vertex][next].flow_ += flowUnit;
      adjacencyMatrix_[next][vertex].flow_ -= flowUnit;
      return flowUnit;
    }
  }
  return 0;
}

bool Graph::bfs() {
  std::queue<int> vertexes;
  vertexes.push(startVertex_);
  distances_ = std::vector<int>(adjacencyMatrix_.size(), -1);
  distances_[startVertex_] = 0;

  while (!vertexes.empty()) {
    int parent = vertexes.front();
    vertexes.pop();

    for (int child = 0; child < adjacencyMatrix_[parent].size(); ++child) {
      if (distances_[child] == -1 &&
          adjacencyMatrix_[parent][child].flow_ < adjacencyMatrix_[parent][child].capacity_ &&
          adjacencyMatrix_[parent][child].capacity_ > 0) {

        distances_[child] = distances_[parent] + 1;
        vertexes.push(child);
      }
    }
  }
  return distances_[endVertex_] != -1;
}