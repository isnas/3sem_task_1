#ifndef MAXFLOW_GRAPH_H
#define MAXFLOW_GRAPH_H

#include <vector>
#include <climits>
#include <assert.h>
#include <queue>

struct Edge {
  Edge() {}
  int capacity_ = 0;
  int flow_ = 0;
};

class Graph {
 private:
  std::vector<std::vector<Edge>> adjacencyMatrix_;
  std::vector<int> distances_;
  int startVertex_, endVertex_;

  int dfs(int vertex, int flow, std::vector<int>& passableVertexPointer);
  bool bfs();

 public:

  Graph(int size) {
    adjacencyMatrix_ = std::vector<std::vector<Edge>>(size, std::vector<Edge>(size, Edge()) );
  }
  void addEdge(int start, int end, int capacity, bool isDirected = false);
  int dinic(int start, int end);
};


#endif //MAXFLOW_GRAPH_H
