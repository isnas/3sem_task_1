#include "../src/Graph.h"
#include "gtest/gtest.h"

TEST(YandeTest, FirstTest) {
  Graph graph(4);
  graph.addEdge(0, 1, 20);
  graph.addEdge(0, 2, 10);
  graph.addEdge(1, 2, 5);
  graph.addEdge(1, 3, 10);
  graph.addEdge(2, 3, 20);

  ASSERT_EQ(graph.dinic(0, 3), 25);
}

TEST(MyTest, FirstTest) {
  Graph graph(5);
  graph.addEdge(0, 1, 5);
  graph.addEdge(0, 2, 5);
  graph.addEdge(0, 3, 5);
  graph.addEdge(2, 3, 10);
  graph.addEdge(3, 4, 7);
  graph.addEdge(2, 4, 2);

  ASSERT_EQ(graph.dinic(0, 4), 9);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}