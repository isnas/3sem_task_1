#include "../src/Graph.h"
#include "gtest/gtest.h"

TEST(YandexTest, FirstTest) {
  std::vector<std::vector<int> > matrix =
      {{  0,   5,   9, 100},
       {100,   0,   2,   8},
       {100, 100,   0,   7},
       {  4, 100, 100,   0}
      };
  std::vector<std::vector<int> > result =
      {{ 0,  5,  7, 13},
       {12,  0,  2,  8},
       {11, 16,  0,  7},
       { 4,  9, 11,  0}
      };
  Graph myGraph(matrix);
  myGraph.relax();

  ASSERT_EQ(myGraph.getMatrix(), result);
}

TEST(MyTest, FirstTest) {
  std::vector<std::vector<int> > matrix =
      {{  0,   1, 100, 100},
       {100,   0,   1, 100},
       {100, 100,   0,   1},
       {  1, 100, 100,   0}
      };
  std::vector<std::vector<int> > result =
      {{0, 1, 2, 3},
       {3, 0, 1, 2},
       {2, 3, 0, 1},
       {1, 2, 3, 0}
      };
  Graph myGraph(matrix);
  myGraph.relax();

  ASSERT_EQ(myGraph.getMatrix(), result);
}

TEST(MyTest, SecondTest) {
  std::vector<std::vector<int> > matrix =
      {{ 0,  1, 10},
       {10,  0,  1},
       { 1,  1,  0}
      };
  std::vector<std::vector<int> > result =
      {{0, 1, 2},
       {2, 0, 1},
       {1, 1, 0}
      };
  Graph myGraph(matrix);
  myGraph.relax();

  ASSERT_EQ(myGraph.getMatrix(), result);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}