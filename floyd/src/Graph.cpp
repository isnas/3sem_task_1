#include "Graph.h"

void Graph::scan(std::istream &in) {
  int vertTotal = adjacencyMatrix.size();
  for(int firstInd = 0; firstInd < vertTotal; ++firstInd)
    for(int secondInd = 0; secondInd < vertTotal; ++secondInd)
      in >> adjacencyMatrix[firstInd][secondInd];
}

void Graph::print(std::ostream &out) {
  int vertTotal = adjacencyMatrix.size();
  for(int firstInd = 0; firstInd < vertTotal; ++firstInd) {
    for(int secondInd = 0; secondInd < vertTotal; ++secondInd)
      out << adjacencyMatrix[firstInd][secondInd] << ' ';

    out << std::endl;
  }
}

void Graph::relax() {
  int vertTotal = adjacencyMatrix.size();
  for (int middleInd = 0; middleInd < vertTotal; ++middleInd)
    for (int firstInd = 0; firstInd < vertTotal; ++firstInd)
      for (int secondInd = 0; secondInd < vertTotal; ++secondInd)
        adjacencyMatrix[firstInd][secondInd] =
            std::min(adjacencyMatrix[firstInd][secondInd],
                     adjacencyMatrix[firstInd][middleInd] +
                     adjacencyMatrix[middleInd][secondInd]);
}