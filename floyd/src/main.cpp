#include <iostream>
#include "Graph.h"


int main() {
  size_t vertTotal;
  std::cin >> vertTotal;

  Graph graph(vertTotal);

  graph.scan(std::cin);

  graph.relax();

  graph.print(std::cout);

  return 0;
}