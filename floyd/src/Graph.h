#ifndef FLOYD_GRAPH_H
#define FLOYD_GRAPH_H

#include <vector>
#include <fstream>

class Graph {
 public:
  Graph(int vertTotal) {
    adjacencyMatrix = std::vector<std::vector<int> >(vertTotal, std::vector<int>(vertTotal));
  }
  Graph(const std::vector<std::vector<int> >& graph) {
    adjacencyMatrix = graph;
  }
  void scan(std::istream& in);
  void print(std::ostream& out);
  void relax();
  std::vector<std::vector<int> > getMatrix() { return adjacencyMatrix; }
 private:
  std::vector<std::vector<int> > adjacencyMatrix;
};

#endif //FLOYD_GRAPH_H
