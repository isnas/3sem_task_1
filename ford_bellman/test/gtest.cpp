#include "../src/Graph.h"
#include "gtest/gtest.h"

TEST(YandexTest, FirstTest) {
  Graph myGraph(2);
  myGraph.getRoads(
      {10.0,
            0.09
      });
  myGraph.changeEdges();

  ASSERT_FALSE(myGraph.solve());
}

TEST(YandexTest, SecondTest) {
  Graph myGraph(4);
  myGraph.getRoads(
      {       32.1, 1.50, 78.66,
       0.03,        0.04,  2.43,
       0.67, 21.22,       51.89,
       0.01,    -1, 0.02
      });
  myGraph.changeEdges();

  ASSERT_TRUE(myGraph.solve());
}

TEST(MyTest, FirstTest) {
  Graph myGraph(2);
  myGraph.getRoads(
      {       51.0, 1.50,
       0.01,        2.00,
       0.60,  0.6,
      });
  myGraph.changeEdges();

  ASSERT_TRUE(myGraph.solve());
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}