#include "Graph.h"

void Graph::getRoads(const std::vector<double>& roads_) {
  roads.clear();
  double weight;
  int counter = 0;
  for (int edgeInd1 = 0; edgeInd1 < vertexTotal; ++edgeInd1) {
    for (int edgeInd2 = 0; edgeInd2 < vertexTotal; ++edgeInd2) {
      if (edgeInd1 == edgeInd2)
        continue;

      weight = roads_[counter++];

      if (weight == -1)
        continue;

      roads.push_back({edgeInd1, edgeInd2, weight});
    }
  }
}

void Graph::readRoads(std::istream &in) {
  double weight;
  roads.clear();
  for (int edgeInd1 = 0; edgeInd1 < vertexTotal; ++edgeInd1) {
    for (int edgeInd2 = 0; edgeInd2 < vertexTotal; ++edgeInd2) {
      if (edgeInd1 == edgeInd2)
        continue;

      in >> weight;
      if (weight == -1)
        continue;

      roads.push_back({edgeInd1, edgeInd2, weight});
    }
  }
}

void Graph::changeEdges() {
  for (int edgeInd = 0; edgeInd < roads.size(); ++edgeInd)
    roads[edgeInd].weight = -log(roads[edgeInd].weight);
}

bool Graph::solve() {
  std::vector<long double> dist(vertexTotal);

  Edge edge;
  bool isNegativeCycle = false;
  for (int vertexInd = 0; vertexInd < vertexTotal; ++vertexInd) {
    for (int edgeInd = 0; edgeInd < roads.size(); ++edgeInd) {
      edge = roads[edgeInd];

      if (dist[edge.end] > dist[edge.start] + edge.weight) {
        dist[edge.end] = dist[edge.start] + edge.weight;

        if (vertexInd == vertexTotal - 1)
          isNegativeCycle = true;
      }
    }
  }

  return isNegativeCycle;
}