#ifndef FORD_BELLMAN_GRAPH_H
#define FORD_BELLMAN_GRAPH_H

#include <vector>
#include <fstream>
#include <math.h>

class Graph {
 public:

  struct Edge {
    int start;
    int end;
    long double weight;
  };

  Graph(int vertexTotal_) : vertexTotal(vertexTotal_) {}
  void getRoads(const std::vector<double>& roads);
  void readRoads(std::istream& in);

  void changeEdges();
  bool solve();

 private:
  int vertexTotal;
  std::vector<Edge> roads;
};

#endif //FORD_BELLMAN_GRAPH_H
