#include <iostream>

#include "Graph.h"

int main() {

  size_t size;
  std::cin >> size;

  Graph graph(size);

  graph.readRoads(std::cin);

  graph.changeEdges();

  if (graph.solve()) {
    std::cout << "YES" << std::endl;
  } else {
    std::cout << "NO"  << std::endl;
  }
  return 0;
}