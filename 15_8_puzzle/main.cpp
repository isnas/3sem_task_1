#include <iostream>
#include <fstream>
#include <vector>
#include <memory>
#include <climits>

#ifdef __linux__
#include <unistd.h>
#endif
#ifdef _WIN32
# include <direct.h>
#endif

static const int TABLE_SIZE =  4;

const std::array<std::pair<short, short>, 4> MOVES {std::make_pair(0, 1), std::make_pair(0, -1),
                                                    std::make_pair(1, 0), std::make_pair(-1, 0)};

struct Result {
  int length;
  std::string way;
};

class State {
 public:

  State(std::array<std::array<short, TABLE_SIZE>, TABLE_SIZE>& board_,
        std::pair<short, short> nullPosition_, char letter_ = '?',
        int level_ = 0) : board(board_), nullPosition(nullPosition_), level(level_) {
    letter = letter_;
  }

  bool hasSolution();
  int getHeuristic();
  std::vector< std::shared_ptr<State> > getNeighbours();
  bool isGoal();

  int getLevel() { return level; }
  char getLetter() { return letter; }

 private:
  std::array<std::array<short, TABLE_SIZE>, TABLE_SIZE> board;
  std::pair<short, short> nullPosition;
  int level;
  char letter;
};

int State::getHeuristic() {
  int manhattanDistance = 0;

  for (short boardIndFirst = 0; boardIndFirst < board.size(); ++boardIndFirst) {
    for (short boardIndSecond = 0; boardIndSecond < board.size(); ++boardIndSecond) {
      short number = board[boardIndFirst][boardIndSecond];
      if (!number)
        continue;

      short homeFirst  = (number - 1) / (short) board.size();
      short homeSecond = (number - 1) % (short) board.size();

      manhattanDistance += std::abs(homeFirst - boardIndFirst) + std::abs(homeSecond - boardIndSecond);
    }
  }

  return manhattanDistance + level;
}

char opposite(char s) {
  if (s == 'R')
    return 'L';
  if (s == 'L')
    return 'R';
  if (s == 'D')
    return 'U';
  return 'D';
}

std::vector< std::shared_ptr<State> > State::getNeighbours(){
  std::vector< std::shared_ptr<State> > neighboursResult;

  char directions[4] = {'R', 'L', 'D', 'U'};
  for (int directionInd = 0; directionInd < MOVES.size(); ++directionInd) {
    std::array<std::array<short, TABLE_SIZE>, TABLE_SIZE> temp_board = board;

    bool firstLineInside  = (nullPosition.first  + MOVES[directionInd].first  < board.size()) &&
        (nullPosition.first  + MOVES[directionInd].first  > -1);

    bool secondLineInside = (nullPosition.second + MOVES[directionInd].second < board.size()) &&
        (nullPosition.second + MOVES[directionInd].second > -1);

    bool directionIsGood = true;
    if (letter == opposite(directions[directionInd]))
      directionIsGood = false;

    if (firstLineInside && secondLineInside && directionIsGood) {
      int newFirstNullOffset   = nullPosition.first  + MOVES[directionInd].first;
      int newSecondNullOffset  = nullPosition.second + MOVES[directionInd].second;
      int firstNullOffset      = nullPosition.first;
      int secondNullOffset     = nullPosition.second;

      std::swap(temp_board[newFirstNullOffset][newSecondNullOffset],
                temp_board[firstNullOffset][secondNullOffset]);

      neighboursResult.push_back(std::make_shared<State>(temp_board,
                                                         std::make_pair(newFirstNullOffset, newSecondNullOffset),
                                                         directions[directionInd], level + 1));
    }
  }

  return neighboursResult;
}

bool State::hasSolution() {
  std::vector<short> temp_array;
  for (short firstInd = 0; firstInd < board.size(); ++firstInd)
    for (short secondInd = 0; secondInd < board.size(); ++secondInd)
      temp_array.push_back(board[firstInd][secondInd]);

  int sum = 0;
  for (short firstInd = 0; firstInd < temp_array.size(); ++firstInd) {
    if (!temp_array[firstInd])
      continue;

    for (short secondInd = 0; secondInd < firstInd; ++secondInd)
      if (temp_array[secondInd] > temp_array[firstInd])
        ++sum;
  }

  if (board.size() % 2 == 1)
    return sum % 2 == 0;

  for (short arrayInd = 0; arrayInd < temp_array.size(); ++arrayInd)
    if (temp_array[arrayInd] == 0)
      sum += arrayInd / board.size();

  return sum % 2 == 1;
}

bool State::isGoal() {
  bool equal = true;
  for (short firstInd = 0; firstInd < board.size(); ++firstInd)
    for (short secondInd = 0; secondInd < board.size(); ++secondInd)
      if (board[firstInd][secondInd] != (firstInd * board.size() + secondInd + 1) % (TABLE_SIZE * TABLE_SIZE)){
        equal = false;
        break;
      }

  return equal;
}

int search(std::shared_ptr<State> state, int bound, std::string& answer, bool& success) {
  int heuristic = state->getHeuristic();

  if (heuristic > bound)
    return heuristic;

  if (state->isGoal()) {
    int size = state->getLevel();
    answer = std::string(size, '?');
    answer[size - 1] = state->getLetter();
    success = true;
    return INT_MAX;
  }

  int min = INT_MAX;
  std::vector< std::shared_ptr<State> > neighbours = state->getNeighbours();
  for (auto neighbourIt : neighbours) {

    int newBound = search(neighbourIt, bound, answer, success);

    if (success) {
      int size = state->getLevel();
      if (size != 0)
        answer[size - 1] = state->getLetter();

      return INT_MAX;
    }

    if (newBound < min)
      min = newBound;
  }

  return min;
}

std::shared_ptr<Result> solve(std::array<std::array<short, TABLE_SIZE>, TABLE_SIZE>& board, std::pair<short, short>& nullPosition) {
  Result result;
  std::shared_ptr<State> start = std::make_shared<State>(board, nullPosition);

  if (!start->hasSolution()) {
    result.length = -1;
    return std::make_shared<Result>(result);
  }

  int bound = start->getHeuristic();

  bool success = false;
  std::string answer;
  while (!success) {
    bound = search(start, bound, answer, success);
  }

  result.way = answer;
  result.length = answer.size();

  return std::make_shared<Result>(result);
}

void scanTable(std::istream& in, std::array<std::array<short, TABLE_SIZE>, TABLE_SIZE>& table, std::pair<short, short>& nullPosition) {
  short nullFirstPosition, nullSecondPosition;

  char letter = ',';
  for (short firstInd = 0; firstInd < table.size(); ++firstInd) {
    for (short secondInd = 0; secondInd < table.size(); ++secondInd) {
      in >> table[firstInd][secondInd];
      in >> letter;

      if (table[firstInd][secondInd] == 0) {
        nullFirstPosition  = firstInd;
        nullSecondPosition = secondInd;
      }
    }
  }

  nullPosition = std::make_pair(nullFirstPosition, nullSecondPosition);
}

int main() {

#ifdef __linux__
  chdir(PROJECT_DIRECTORY);
#endif
#ifdef _WIN32
  _chdir(PROJECT_DIRECTORY);
#endif

  std::ifstream fin("puzzle.in");
  std::ofstream fout("puzzle.out");

  std::array<std::array<short, TABLE_SIZE>, TABLE_SIZE> table;
  std::pair<short, short> nullPosition;

  scanTable(fin, table, nullPosition);

  std::shared_ptr<Result> result = solve(table, nullPosition);

  fout << result->length << std::endl;

  if (result->length != -1) {
    fout << result->way << std::endl;
  }

  fin.close();
  fout.close();

  return 0;
}